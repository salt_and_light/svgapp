import React, { Component } from 'react';
import SVGGrid from './SVGGrid';
import DrawShape from '../DrawShape/DrawShape';
import { Consumer } from '../AppProvider';
import './Canvas.css';

class Canvas extends Component {
    constructor(props) {
        super(props);

        this.svg = null;

        this.state = {
            mousedown: false,
            x: 0,
            y: 0,
            x2: 0,
            y2: 0,
            width: 0,
            height: 0,
        }
    }

    getCTM(x, y) {
        const CTM = this.svg.getScreenCTM();
        return {
            x: (x - CTM.e) / CTM.a,
            y: (y - CTM.f) / CTM.d,
        }
    }

    startDrag(e) {
        let startPoint = this.svg.createSVGPoint();
        const { x, y } = this.getCTM(e.clientX, e.clientY);
        startPoint.x = x;
        startPoint.y = y;
        this.setState({
            mousedown: true,
            x: startPoint.x,
            y: startPoint.y,
        })
    }

    stopDrag(e) {
        let endPoint = this.svg.createSVGPoint();
        const { x, y } = this.getCTM(e.clientX, e.clientY);
        endPoint.x = x;
        endPoint.y = y;
        this.setState({
            mousedown: false,
            x2: endPoint.x,
            y2: endPoint.y,
            width: endPoint.x - this.state.x,
            height: endPoint.y - this.state.y,
        })
    }

    handleDragging(e) {
        let endPoint = this.svg.createSVGPoint();
        const { x, y } = this.getCTM(e.clientX, e.clientY);
        endPoint.x = x;
        endPoint.y = y;
        const width = endPoint.x - this.state.x;
        const height = endPoint.y - this.state.y;
        if(this.state.mousedown) {
            this.setState({
                width: Math.abs(width),
                height: Math.abs(height),
            })
        }
    }

    render() {
        return (
            <Consumer>{(context) => (
                <div className="canvas-wrapper">
                    <svg
                        width="100%"
                        height="100%"
                        ref={svg => this.svg = svg}
                        onMouseDown={e => this.startDrag(e)}
                        onMouseUp={e => this.stopDrag(e)}
                        onMouseMove={e => this.handleDragging(e)}
                    >
                        <SVGGrid />
                        <DrawShape shape={context.shape} {...this.state} />
                    </svg>
                </div>
            )}
            </Consumer>
        )
    }
}
export default Canvas;