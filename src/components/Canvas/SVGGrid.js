import React from 'react';

const SVGGrid = () => {
    return (
        <svg width="100%" height="100%">
            <defs>
                <pattern id="grid" width="100" height="100" patternUnits="userSpaceOnUse">
                    <path d="M 100 0 L 0 0 0 100" fill="none" stroke="#EEEEEE" strokeWidth="2" />
                </pattern>
            </defs>
            <rect width="100%" height="100%" fill="url(#grid)" />
        </svg>
    )
}

export default SVGGrid;