import React, { Fragment } from 'react';

const DrawShape = (props) => {
    const {shape} = props;
    return (
        <Fragment>
            {shape === 'rect' && <rect {...props} />}
            {shape === 'circle' && <circle cx={props.x} cy={props.y} r={props.width} />}
            {shape === 'ellipse' && <ellipse cx={props.x} cy={props.y} rx={props.width*2} ry={props.height/2} />}
            {shape === 'line' && <line x1={props.x} y1={props.y} x2={props.width} y2={props.height} stroke="black" />}
        </Fragment>
    ) 
}

export default DrawShape;