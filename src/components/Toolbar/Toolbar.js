import React, { Fragment } from 'react';
import LeftArrow from '../../icons/LeftArrow'; 
import Square from '../../icons/Square'; 
import Circle from '../../icons/Circle'; 
import Ellipse from '../../icons/Ellipse'; 
import LineIcon from '../../icons/LineIcon'; 
import Crosshair from '../../icons/Crosshair'; 
import { Consumer } from '../AppProvider';
import './Toolbar.css';

const Toolbar = () => (
    <Consumer>{(context) => (
        <Fragment>
            <div className="toolbar">
                <p>TOOLS</p>
                <div className="toolbar-container">
                    <button className="toolbar-shape-button">
                        <LeftArrow width="20" height="20" />
                    </button>
                    <button className="toolbar-shape-button">
                        <Crosshair viewBox="0 0 20 20" width="20" height="20" />
                    </button>
                </div>
                <p>SHAPES</p>
                <div className="toolbar-container">
                    <button className="toolbar-shape-button" id="rect" onClick={context.setShape}>
                        <Square viewBox="0 0 20 20" width="20" height="20" />
                    </button>
                    <button className="toolbar-shape-button" id="circle" onClick={context.setShape}>
                        <Circle viewBox="0 0 20 20" width="20" height="20" />
                    </button>
                    <button className="toolbar-shape-button" id="ellipse" onClick={context.setShape}>
                        <Ellipse viewBox="0 0 20 20" width="20" height="20" />
                    </button>
                    <button className="toolbar-shape-button" id="line" onClick={context.setShape}>
                        <LineIcon viewBox="0 0 15 15" width="20" height="20" />
                    </button>
                </div>
                <p>PROPERTIES</p>
            </div>
        </Fragment>
    )}
    </Consumer>
)

export default Toolbar;