import React, { Component } from 'react';

export const {Consumer, Provider} = React.createContext()

class AppProvider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            shape: '',
            color: '',
            setShape: (e) => {
                console.log('event', e.currentTarget.id);
                this.setState({
                    shape: e.currentTarget.id
                })
            }
        };
    }
    
    render () {
      return (
        <Provider value={this.state}>
          {this.props.children}
        </Provider>
      )
    }
  }

  export default AppProvider;