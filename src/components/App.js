import React, { Fragment } from 'react';
import Toolbar from './Toolbar/Toolbar';
import Canvas from './Canvas/Canvas';
import AppProvider from './AppProvider';
import './App.css';

const App = () => {
    return(
        <AppProvider>
            <div className="app-wrapper">
                <Toolbar />
                <Canvas />
            </div>
        </AppProvider>
    )
}

export default App;