import React from "react";

const SvgCircle = props => (
  <svg width={20} height={20} {...props}>
    <path
      fill="#FFF"
      d="M10 18a8 8 0 100-16 8 8 0 000 16zm0 2C4.477 20 0 15.523 0 10S4.477 0 10 0s10 4.477 10 10-4.477 10-10 10z"
    />
  </svg>
);

export default SvgCircle;
