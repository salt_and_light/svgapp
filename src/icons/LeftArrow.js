import React from "react";

const SvgLeftArrow = props => (
  <svg
    width={10.487}
    height={10.486}
    viewBox="0.757 0.414 10.487 10.486"
    {...props}
  >
    <path
      fill="#FFF"
      d="M2.757 3.828v5.586a1 1 0 01-2 0v-8a.997.997 0 01.994-1h8.006a1 1 0 010 2H4.172l6.778 6.778a1 1 0 11-1.415 1.415L2.757 3.828z"
    />
  </svg>
);

export default SvgLeftArrow;
