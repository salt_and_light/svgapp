import React from "react";

const SvgLineIcon = props => (
  <svg width={14.849} height={14.849} {...props}>
    <path
      fill="none"
      stroke="#FFF"
      strokeWidth={2}
      strokeMiterlimit={10}
      d="M.354 14.496L14.496.354"
    />
  </svg>
);

export default SvgLineIcon;
