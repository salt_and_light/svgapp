import React from "react";

const SvgDryClean = props => (
  <svg width={512} height={512} {...props}>
    <path
      d="M256 0C114.833 0 0 114.844 0 256s114.833 256 256 256 256-114.844 256-256S397.167 0 256 0zm0 490.667C126.604 490.667 21.333 385.396 21.333 256S126.604 21.333 256 21.333 490.667 126.604 490.667 256 385.396 490.667 256 490.667z"
      data-original="#000000"
      className="dry-clean_svg__active-path"
      data-old_color="#000000"
      fill="#FFF"
    />
  </svg>
);

export default SvgDryClean;
