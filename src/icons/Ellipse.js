import React from "react";

const SvgEllipse = props => (
  <svg width={20} height={20} {...props}>
    <path
      fill="#FFF"
      d="M10 15.384c4.418 0 8-2.463 8-5.503s-3.582-5.504-8-5.504-8 2.464-8 5.504c0 3.04 3.582 5.503 8 5.503zm0 1.377c-5.523 0-10-3.08-10-6.88S4.477 3 10 3s10 3.08 10 6.88-4.477 6.881-10 6.881z"
    />
  </svg>
);

export default SvgEllipse;
