import React from "react";

const SvgCrosshair = props => (
  <svg width={20} height={20} {...props}>
    <path
      fill="#FFF"
      d="M19 0a1 1 0 011 1v6a1 1 0 11-2 0V3.414L11.414 10 18 16.586V13a1 1 0 112 0v6a1 1 0 01-1 1h-6a1 1 0 110-2h3.586L10 11.414 3.414 18H7a1 1 0 110 2H1a1 1 0 01-1-1v-6a1 1 0 012 0v3.586L8.586 10 2 3.414V7a1 1 0 01-2 0V1a1 1 0 011-1h6a1 1 0 010 2H3.414L10 8.586 16.586 2H13a1 1 0 010-2h6z"
    />
  </svg>
);

export default SvgCrosshair;
